FROM alpine

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]
RUN apk add --no-cache curl tar git && \
  curl -fsSL https://dotfiles.pidrak.in | sh -s -- --no-package
COPY . /opt/dotfiles.git
