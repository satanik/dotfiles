# -*- mode: conf -*-
# vim:ft=.tmux.conf

###################
### TMUX CONFIG ###
###################

run-shell "tmux setenv -g TMUX_VERSION $(tmux -V | cut -d' ' -f2 | sed 's/[^0-9.]//g')"

# reset all options
source-file ~/.tmux/reset

# use vim keybindings in copy mode
set-option -g mode-keys vi
# bind-key -T copy-mode-vi v send-keys -X begin-selection
# bind-key -T copy-mode-vi y send-keys -X copy-selection
# bind-key -T copy-mode-vi r send-keys -X rectangle-toggle

# use bash as default shell
set-option -g default-shell $SHELL

set -g default-terminal "screen-256color"

# use a longer display time for messages
set-option -g display-time 2000

# prevent exit from tmux on destroying sessions
set-option -g detach-on-destroy off

######################
### DESIGN CHANGES ###
######################

# Refresh status line every second
set -g status-interval 1

#################
### STATUSBAR ###
#################

#D (pane id)
#F (window flags)
#H (hostname)
#I (window index)
#P (pane index)
#S (session index)
#T (pane title)
#W (currnet task like vim if editing a file in vim or zsh if running zsh)

# set -g status-attr none
set -g status-style fg=white,bg=default,bold
# set -g status-left "[#S] "
# set -g status-left '[#S:#(echo $USER)@#h] #[fg=blue,bright]#P#[default]:#W #{?client_prefix,#[fg=red]#[bright],}◉ #[default] #{?window_zoomed_flag,#[fg=yellow]ZOOM #[default],}#{?pane_synchronized,#[fg=blue]SYNC #[default],} '
set -g status-left '[#S:#(whoami)@#h] #{?pane_synchronized,#[fg=blue]SYNC #[default],}'
# set -g status-right "#(~/.tmux/plugins/tmux-continuum/scripts/continuum_save.sh) \"#{=21:pane_title}\" %H:%M %d-%b-%y"
set -g status-right '#(~/.tmux/plugins/tmux-continuum/scripts/continuum_save.sh) %d/%m #[fg=blue] %T'


set -g status-position bottom
set -g status-justify left
set -g status-right-length 150
set -g status-left-length 200

############
### MODE ###
############

setw -g clock-mode-colour blue
setw -g mode-style fg=black,bg=yellow,none

#############
### PANES ###
#############

setw -g pane-border-style fg=default,bg=default
setw -g pane-active-border-style fg=default,bg=default

###############
### WINDOWS ###
###############

setw -g window-status-current-style fg=default,bg=default,none
setw -g window-status-current-format "#[fg=black,bright]#{?client_prefix,#[bg=red],#[bg=yellow]} #{?window_zoomed_flag,🗖 ,}#I:#W #[default]"
#setw -g window-status-current-format ''

setw -g window-status-style fg=default,bg=default,none
setw -g window-status-format "#{?window_activity_flag, ,}#{?window_zoomed_flag,🗖 ,}#I:#W#{?window_activity_flag, ,}"
#setw -g window-status-format ''

setw -g window-status-bell-style fg=default,bg=default,reverse

setw -g window-status-activity-style fg=red,bold,bg=default,reverse

################
### MESSAGES ###
################

set -g message-style fg=black,bg=yellow,none

################
### BINDINGS ###
################

bind e send-keys "vim ~/.tmux.conf" Enter \; display "Open config..."
bind r source-file ~/.tmux.conf \; display "Config reloaded..."
bind P attach -c "#{pane_current_path}" \; display "New working directory: #{pane_current_path}"
bind m set-window-option synchronize-panes\; display "synchronize-panes is now #{?pane_synchronized,on,off}"
bind C-p run "tmux respawn-pane -k"

# send prefix to nested tmux sessions
bind-key -n C-y send-prefix

# terminal bindings
# replace C-k with C-d because C-k is used for jumping between panes
bind-key -n M-k send-keys C-k
bind-key -n M-u send-keys C-u
#bind-key -n C-x send-keys C-l

bind-key W display-popup -T "TMS Window" -E "~/.tmux/tms window"
bind-key S display-popup -T "TMS Session" -E "~/.tmux/tms session"

#; clear-history

# Set window notifications
setw -g monitor-activity on
set -g visual-activity on

# Allow the arrow key to be used immediately after changing windows
set-option -g repeat-time 0

# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'

# Other examples:
# set -g @plugin 'github_username/plugin_name'
# set -g @plugin 'git@github.com/user/plugin'
# set -g @plugin 'git@bitbucket.com/user/plugin'
set -g @plugin 'christoomey/vim-tmux-navigator'
# set -g @plugin 'tmux-plugins/tmux-resurrect'
# set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-fpp'

# Restore Vim sessions
set -g @resurrect-strategy-vim 'session'
# Restore Panes
set -g @resurrect-capture-pane-contents 'on'
# Hook to remove extra session
set -g @resurrect-hook-pre-restore-pane-processes 'tmux switch-client -n && tmux kill-session -t=0'
# Restore bash history
# set -g @resurrect-save-bash-history 'on'

set -g @continuum-boot 'on'
set -g @continuum-restore 'on'

# Load os-specific configurations
if-shell '[[ "$OSTYPE" == "darwin"* ]]' 'source-file ~/.tmux/.tmux.conf.darwin'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
