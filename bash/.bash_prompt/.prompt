#!/usr/bin/env bash

# Various variables you might want for your PS1 prompt instead
#Time12h="\T"
#Time12a="\@"
#PathShort="\W"
PathFull="\w"
#NewLine="\n"
#Jobs="\j"
User="\u"
Host="\h"
#Subdomain="\H"

source "${HOME}/.bash_prompt/.theme"

if [[ $OSTYPE == darwin* ]]; then
  fail_symbol="${_FAIL_COLOR}⨉"
  success_symbol="${_SUCCESS_COLOR}√"
elif [[ $OSTYPE == linux* ]]; then
  fail_symbol="${_FAIL_COLOR}⨯"
  success_symbol="${_SUCCESS_COLOR}✓"
fi

function last_success() {
  if [[ $? == 0 ]]; then
    echo -e "$success_symbol"
  else
    echo -e "$fail_symbol"
  fi
}

# Define the default prompt terminator character '$'
function get_jobs() {
  local running
  running=$(jobs -rp | wc -l | xargs)
  [[ $running -gt 0 ]] && echo -n " | ${running} job(s)"
}
if [[ "$UID" == 0 ]]; then
  symbol="${_JOBS_ROOT_COLOR}[\!\$(get_jobs)]#"
else
  symbol="${_JOBS_COLOR}[\!\$(get_jobs)]\$"
fi

source "${HOME}/.bash_prompt/.git_prompt"
source "${HOME}/.bash_prompt/.kubectl_prompt"
source "${HOME}/.bash_prompt/.venv_prompt"

# Define the bash-prompt
export PS1="\n\
 \$(last_success) \
${_USER_COLOR}${User} \
${_PREPOSITION_COLOR}at \
${_DEVICE_COLOR}${Host} \
${_PREPOSITION_COLOR}in \
${_DIR_COLOR}${PathFull}\
\$( \
  [[ -n \"\$(type -t print_git_prompt)\" ]] && \
  [[ \"\$(type -t print_git_prompt)\" = \"function\" ]] && \
  echo -e \" \$(print_git_prompt)\" \
)\n\
\$( \
  [[ -n \"\$(type -t print_kubectl_context)\" ]] && \
  [[ \"\$(type -t print_kubectl_context)\" = \"function\" ]] && \
  echo -e \" \$(print_kubectl_context)\" \
)\
\$( \
  [[ -n \"\$(type -t print_venv_prompt)\" ]] && \
  [[ \"\$(type -t print_venv_prompt)\" = \"function\" ]] && \
  echo -e \"\$(print_venv_prompt)\" \
)\
${symbol} ${_RESET}"
