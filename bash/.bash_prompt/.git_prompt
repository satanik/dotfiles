#!/usr/bin/env bash

# Set up symbols
if [[ $OSTYPE == darwin* ]]; then
  submodule_symbol="${_BRANCH_STATUS_COLOR}⊆"
  ahead_symbol="${_BRANCH_STATUS_COLOR}↑"
  behind_symbol="${_BRANCH_STATUS_COLOR}↓"
  stashed_symbol="${_STASHED_COLOR}⚑"
  conflict_symbol="${_CONFLICT_COLOR}✖"
  staged_symbol="${_STAGED_COLOR}●"
  modified_symbol="${_MODIFIED_COLOR}✚"
  clean_symbol="${_CLEAN_COLOR}✔"
  untracked_symbol="${_UNTRACKED_COLOR}…"
elif [[ $OSTYPE == linux* ]]; then
  submodule_symbol="${_BRANCH_STATUS_COLOR}${bold}⊆"
  ahead_symbol="${_BRANCH_STATUS_COLOR}↑ "
  behind_symbol="${_BRANCH_STATUS_COLOR}↓ "
  stashed_symbol="${_STASHED_COLOR}⚑"
  conflict_symbol="${_CONFLICT_COLOR}⨯ "
  staged_symbol="${_STAGED_COLOR}🞋 "
  modified_symbol="${_MODIFIED_COLOR}${bold}+"
  clean_symbol="${_CLEAN_COLOR}✓"
  untracked_symbol="${_UNTRACKED_COLOR}…"
fi


function get_branch() {
  # On branches, this will return the branch name
  # On non-branches, (no branch)
  ref="$(git symbolic-ref HEAD 2> /dev/null | sed -e 's/refs\/heads\///')"
  if [[ ! -z ${ref} ]]; then
    echo "${ref}"
  else
    echo "no-branch"
  fi
}

function get_superproject() {
  superproject=$(git rev-parse --show-superproject-working-tree)
  echo "${superproject}"
}

function get_num_remotes() {
  echo "$(git remote | wc -l | xargs)"
}

function has_remote_tracked() {
  branch="$1"
  remote="$(git config "branch.${branch}.remote")"
  [[ $? -eq 0 && $remote != '.' ]]
}

function get_distance_ahead_behind() {
  branch="$1"
  remotes="$(git remote)"
  for remote in $remotes; do
    remote_branch="${remote}/${branch}"
    if [[ $(git rev-parse --verify ${remote_branch} 2>/dev/null) ]]; then
      ahead="$(git rev-list --left-right --count ${branch}...${remote_branch} 2>/dev/null | awk '{ print $1 }')"
      behind="$(git rev-list --left-right --count ${branch}...${remote_branch} 2>/dev/null | awk '{ print $2 }')"
      echo "${ahead} ${behind}"
    fi
  done
}

function get_num_stashed() {
  echo "$(git stash list | wc -l | xargs)"
}

function get_num_conflict() {
  echo "$(git diff --name-only --diff-filter=U | wc -l | xargs)"
}

function get_has_conflict() {
  if [[ -f "$(git rev-parse --git-dir 2>/dev/null)/MERGE_HEAD" ]]; then
    echo "1"  # Indicates a merge conflict is ongoing
  else
    echo "0"  # Indicates no conflict
  fi
}



function get_num_untracked() {
  echo "$(git ls-files --others --exclude-standard $(git rev-parse --show-toplevel) | wc -l | xargs)"
}

function get_num_modified() {
  # Update index without creating any objects
  git update-index -q --ignore-submodules --refresh

  # Use diff-files to get a quick list of modified files
  modified=$(git diff-files --name-only | wc -l | xargs)

  # Get unmerged files
  unmerged=$(git ls-files --unmerged $(git rev-parse --show-toplevel) | awk '{ print $NF }' | uniq | wc -l | xargs)

  # Calculate the total number of modified files, excluding unmerged files
  total_modified=$((modified - unmerged))

  echo "${total_modified}"
}

function get_num_staged() {
  echo "$(git diff --cached --numstat --diff-filter=u | wc -l | xargs)"
}

function is_on_git() {
  git rev-parse 2> /dev/null
}

function get_status() {
  branch=$1
  remotes="$(get_num_remotes)"
  has_remote_tracked="$(has_remote_tracked "${branch}"; echo $?)"
  stashed="$(get_num_stashed)"
  conflict="$(get_has_conflict)"
  untracked="$(get_num_untracked)"
  modified="$(get_num_modified)"
  staged="$(get_num_staged)"

  get_distance_ahead_behind "${branch}" | while read -r branch_ahead branch_behind; do
    if [[ ! -z ${branch_behind+x} && $branch_behind -gt 0 ]]; then
      echo -n "${behind_symbol}${branch_behind}"
    fi

    if [[ ! -z ${branch_ahead+x} && $branch_ahead -gt 0 ]]; then
      echo -n "${ahead_symbol}${branch_ahead}"
    fi
  done

  if [[ $remotes -eq 0 ]]; then
    remote_status="${remote_status}L"
  elif [[ $remotes -gt 1 ]]; then
    remote_status="${remote_status}R${remotes}"
  fi

  if [[ $has_remote_tracked -ne 0 ]]; then
    remote_status="${remote_status}U"
  fi

  if [[ ! -z ${remote_status+x} ]]; then
    echo -n "${_REMOTE_STATUS_COLOR}:${remote_status}"
  fi

  if [[ ! -z ${stashed+x} && $stashed -gt 0 ]]; then
    status="${status}${stashed_symbol}${stashed}"
  fi

  if [[ ! -z ${conflict+x} && $conflict -gt 0 ]]; then
    status="${status}${conflict_symbol}"
  fi

  if [[ ! -z ${staged+x} && $staged -gt 0 ]]; then
    status="${status}${staged_symbol}${staged}"
  fi

  if [[ ! -z ${modified+x} && $modified -gt 0 ]]; then
    status="${status}${modified_symbol}${modified}"
  fi

  if [[ -z ${status+x} ]]; then
    status="${clean_symbol}"
  fi

  if [[ ! -z ${untracked+x} && $untracked -gt 0 ]]; then
    status="${status}${untracked_symbol}${untracked}"
  fi

  echo -n "${reset}|${status}"
}

function print_git_prompt() {
  if ! is_on_git || [[ -z "$GIT_PROMPT_SHOW" ]]; then
    return 0
  fi

  # Grab the branch
  branch="$(get_branch)"

  # If there are any branches
  if [[ ! -z $branch ]]; then

    # Add on the git status
    status="$(get_status "$branch")"

    superproject="$(get_superproject)"

    # Echo our output

    echo -n "${_RESET}["

    if [[ ! -z $superproject ]]; then
      echo -n "${submodule_symbol}${_RESET}:"
    fi

    echo -n "${branch}"
    echo -n "${status}"
    echo -n "${_RESET}"

    echo -n "]"
  fi
}

# TODO: maybe we need that at some point
# function get_git_progress() {
#   # Detect in-progress actions (e.g. merge, rebase)
#   # https://github.com/git/git/blob/v1.9-rc2/wt-status.c#L1199-L1241
#   git_dir="$(git rev-parse --git-dir)"

#   # git merge
#   if [[ -f "$git_dir/MERGE_HEAD" ]]; then
#     echo " [merge]"
#   elif [[ -d "$git_dir/rebase-apply" ]]; then
#     # git am
#     if [[ -f "$git_dir/rebase-apply/applying" ]]; then
#       echo " [am]"
#     # git rebase
#     else
#       echo " [rebase]"
#     fi
#   elif [[ -d "$git_dir/rebase-merge" ]]; then
#     # git rebase --interactive/--merge
#     echo " [rebase]"
#   elif [[ -f "$git_dir/CHERRY_PICK_HEAD" ]]; then
#     # git cherry-pick
#     echo " [cherry-pick]"
#   fi
#   if [[ -f "$git_dir/BISECT_LOG" ]]; then
#     # git bisect
#     echo " [bisect]"
#   fi
#   if [[ -f "$git_dir/REVERT_HEAD" ]]; then
#     # git revert --no-commit
#     echo " [revert]"
#   fi
# }
