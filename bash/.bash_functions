#!/bin/usr/env bash

###############################################################################
###                               FUNCTIONS                                 ###
###############################################################################

function source_if() {
  if [[ -f $1 ]]; then
    . "$1"
  else
    error "[$1] does not exist"
  fi
}

function alias_if() {
  local cmd
  cmd=$(echo "$1" | cut -d' ' -f1)
  if type "$cmd" >/dev/null 2>&1; then
    alias "$cmd"="$1"
  else
    error "[$cmd] is missing"
  fi
}

function psf() {
  ps -eo pid,state,user,command | grep -v grep | grep "$1"
}

# find all filteypes in directory structure
function types() {
  find "$1" -type f | perl -ne 'print "$1" if m/\.([^.\/]+)$/' | sort -u
}

function lean() {
  local text=${1:-novalue}

  if type figlet >/dev/null 2>&1; then
    if tty -s; then
      banner=$(figlet -w "$(tput cols)" -f lean "$text")
    else
      banner=$(figlet -w 400 -f lean "$text")
    fi
    if type lolcat >/dev/null 2>&1; then
      lolcat -S 1 -p 1.5 -F "$(LC_ALL="en_US.UTF-8" printf "%0.3f" "$(bc -l <<< "15 / $(tput cols)")")" <(echo "$banner")
    else
      echo "$banner"
      type error >/dev/null 2>&1 && error  "[lolcat] is missing" || echo "[lolcat] is missing"
    fi
  else
    echo "$text"
    type error >/dev/null 2>&1 && error  "[figlet] is missing" || echo "[figlet] is missing"
  fi
}

function init() {
  if [[ (-f ~/.hushlogin) && (-f /etc/motd) ]]; then
    lean "$(cat /etc/motd)"
  fi
}

function reminder_cd() {
  builtin cd "$@" && ({
    if [ -f .cd-reminder ]; then
      lean "$(cat .cd-reminder)"
    fi
  })
}

# a function that prefixes the PATH environment variable with a path if it exists and is not in the PATH yet
function prepend_to_path() {
  if [[ -d $1 ]]; then
    if [[ ":$PATH:" != *":$1:"* ]]; then
      export PATH="$1:$PATH"
    fi
  fi
}

# a function that suffixes the PATH environment variable with a path if it exists and is not in the PATH yet
function append_to_path() {
  if [[ -d $1 ]]; then
    if [[ ":$PATH:" != *":$1:"* ]]; then
      export PATH="$PATH:$1"
    fi
  fi
}

toggle_names=()

register_toggle() {
    toggle_name=$1
    toggle_names+=($toggle_name)
}

toggle_autocomplete() {
    local word=${COMP_WORDS[COMP_CWORD]}
    COMPREPLY=( $(compgen -W "${toggle_names[*]}" -- "${word}") )
}

complete -F toggle_autocomplete toggle

toggle() {
    toggle_name=$1
    if [ -z $toggle_name ]; then
        echo "Please provide a toggle name as the first argument."
        return 1
    fi
    # Call the function toggle_${toggle_name}
    toggle_${toggle_name}
}