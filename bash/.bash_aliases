#!/usr/bin/env bash

###############################################################################
###                                ALIASES                                  ###
###############################################################################

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."

alias grep='grep --color=auto'

alias ll='ls -lahF'
alias la='ls -lhF'

alias msg='printf "'${blue}'%s'${reset}'\n" "$1" >&2'
alias warning='printf "'${yellow}'%s'${reset}'\n" "$1" >&2'
alias error='printf "'${red}'%s'${reset}'\n" "$1" >&2'

if type sudo >/dev/null 2>&1; then
  alias sudo='sudo '
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  # Do something under Mac OS X platform
  alias ll='ls -laheFOG'
  alias la='ls -lheFOG'
elif [[ "$OSTYPE" == "linux"* ]]; then
  # some more ls aliases
  alias ls='ls --color=auto'
  alias ll='ls -lahF --group-directories-first'
  alias la='ls -A'
fi

if [[ -x "${HOME}/.bin/safe_rm" ]]; then
  alias rm="${HOME}/.bin/safe_rm"
fi
if type reminder_cd >/dev/null 2>&1; then
  alias cd=reminder_cd
fi
if type unbuffer >/dev/null 2>&1; then
  alias unbuffer='unbuffer '
else
  error "[unbuffer] is missing"
fi