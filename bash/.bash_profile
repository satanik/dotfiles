#!/usr/bin/env bash

shopt -s extglob

# add color variables
source "${HOME}/.bash_colors"

# add bash aliases
source "${HOME}/.bash_aliases"

# add custom bash functions
source "${HOME}/.bash_functions"

# source .bashrc if this is the one that's loaded first
# source_if ~/.bashrc

if [[ "$OSTYPE" == "darwin"* || "$OSTYPE" == "linux"* ]]; then
  export LC_ALL="en_US.UTF-8"
  export LANG="en_US.UTF-8"
  export LANGUAGE="en_US.UTF-8"
fi

source_if "${HOME}/.bash_history_config"

init

if [[ $OSTYPE == linux* ]]; then
  source_if /usr/share/bash-completion/bash_completion
  if type git-flow >/dev/null 2>&1; then
    source_if /etc/bash_completion.d/git-flow
  fi
fi

source_if ~/.bash_prompt/.prompt

prepend_to_path "/usr/local/bin"
prepend_to_path "/usr/local/sbin"
prepend_to_path "/opt/local/bin"
prepend_to_path "/opt/local/sbin"

###############################################################################
###                          PACKAGE PROFILES                               ###
###############################################################################
if [[ -d "${HOME}/.profiles" ]]; then
  for f in "${HOME}"/.profiles/.[!.]*; do
    source "$f"
  done
fi

if [[ -d "${HOME}/.bin" ]]; then
  # add own binary helpers to path
  prepend_to_path "${HOME}/.bin"
fi