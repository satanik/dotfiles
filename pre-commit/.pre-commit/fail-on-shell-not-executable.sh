#!/usr/bin/env bash
set -e
if [[ -z "${1##*.sh}" ]] && [[ ! -x "$1" ]]; then
  echo "[fail-on-shell-not-executable] file [${1}] has an .sh suffix but is not executable"
  exit 1
fi
