#!/usr/bin/env bash
set -e
git fetch
git status -s -b | grep -q -v behind
