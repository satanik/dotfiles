#!/usr/bin/env sh
set -eu

PACKAGE=$1

if ! type ${PACKAGE} >/dev/null 2>&1; then
  $(dirname $0)/install-brew.sh
  brew install ${PACKAGE} >/dev/null 2>&1
  echo "installed [${PACKAGE}]"
fi