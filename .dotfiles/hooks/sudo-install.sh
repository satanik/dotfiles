#!/usr/bin/env sh
set -eu

if [ "$(uname -s)" = "Linux" ]; then
  if type sudo >/dev/null 2>&1; then
    sudo $(dirname $0)/install.sh $1
  else
    $(dirname $0)/install.sh $1
  fi
elif [ "$(uname -s)" = "Darwin" ]; then
  $(dirname $0)/brew-install.sh $1
fi