#!/usr/bin/env sh
set -eu

PACKAGE=$1

if ! type ${PACKAGE} >/dev/null 2>&1; then
  if type apt >/dev/null 2>&1; then
    apt update
    apt install -y ${PACKAGE} >/dev/null 2>&1
    echo "installed [${PACKAGE}]"
  elif type apk >/dev/null 2>&1; then
    apk --update add --no-cache ${PACKAGE} >/dev/null 2>&1
    echo "installed [${PACKAGE}]"
  elif type dnf >/dev/null 2>&1; then
    dnf install -y ${PACKAGE} >/dev/null 2>&1
    echo "installed [${PACKAGE}]"
  fi
fi