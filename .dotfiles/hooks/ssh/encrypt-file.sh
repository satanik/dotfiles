#!/usr/bin/env sh
set -eu

# Exit if gpg is not installed
if ! type gpg >/dev/null 2>&1; then
    echo "gpg is not installed"
    exit 1
fi

# Exit if parameter count is wrong
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <file>"
    exit 1
fi

gpg --symmetric --cipher-algo AES256 --pinentry-mode loopback $1
